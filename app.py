#Load Libraries
import pandas
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

#load data

url = "http://openstat.psa.gov.ph/sites/default/files/Table%2013.1%28annual%29_0.csv"
names = ['Railway', 'Items', '2010', '2011', '2012', '2013', '2014']
dataset = pandas.read_csv(url, names=names)

#shape
print(dataset.shape)
#head
print(dataset.head(50))
#description
print(dataset.describe())
 	
# class distribution
print(dataset.groupby('Railway').size())
#box and whisker plots
dataset.plot(kind='box', subplots=True, layout=(5,5), sharex=False, sharey=False)
plt.show()
#histogram
#dataset.hist()
#plt.show()
